import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ComunicacionService } from '../comunicacion.service';
import { timeout } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  form = new FormGroup({
    nombre: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z]+$"), Validators.minLength(3)]),
    apellidos: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z]+$")]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
    passwordAgain: new FormControl('', [Validators.required]),
    gender: new FormControl('', [Validators.required]),
    txtEmpPhone: new FormControl('', [Validators.required]),
    preguntaSeguridad: new FormControl('', [Validators.required]),
    segurityAnswer: new FormControl('', [Validators.required]),
  }, {
    validators: this.validaPassIguales
  })

  get f() {
    return this.form.controls;
  }

  constructor(private rutas: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private comunica: ComunicacionService) { }

  ngOnInit(): void {
  }

  validaPassIguales(group: FormGroup) {
    const password = group.controls.password.value;
    const passwordConfirmation = group.controls.passwordAgain.value;
    return password == passwordConfirmation ? null : { passwordNotEqual: true }
  }

  sumbit() {
    this.spinner.show();
    const datos = this.form.value;
    this.comunica.createAccount(datos)
      .pipe(timeout(20000))
      .subscribe((response) => {
        this.validaResponse(response.status, response);
      },
        (response) => {
          this.validaResponse(response.status, response);
        });
  }

  validaResponse(code, response) {
    this.spinner.hide();
    switch (code) {
      case 200:
        this.toastr.success('Cuenta creada');
        this.form.reset();
        this.rutas.navigate(['/login']);
        break;
      default:
        this.toastr.error('Ups', 'Error, al crear su cuenta', {
          timeOut: 3000,
        });
        this.form.reset();
        break;
    }
  }

  login() {
    this.rutas.navigate(['/login']);
  }
  crearCuenta() {
    this.rutas.navigate(['/registro']);
  }

}
