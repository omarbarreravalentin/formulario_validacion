import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComunicacionService {
  url="https://dummyintegracion.herokuapp.com";

  constructor(private httpClient:HttpClient) {

  }

  createAccount=(algo:any):Observable<any>=>{
    const options={
      headers:new HttpHeaders({
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'*',
        'Access-Control-Allow-Methods':'GET, POST, DELETE, PUT',
        'Access-Control-Allow-Headers':'X-Requests-With,content-type'
      }),
      observe: 'response' as 'response'
    };
    console.log("Recibe en el service");
    console.log(algo);
    return this.httpClient.post(`${this.url}/createAccount`,JSON.stringify(algo),options).pipe(map(res=>res));
  }

  validaAccount=(algo):Observable<any>=>{
    const options={
      headers:new HttpHeaders({
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'*',
        'Access-Control-Allow-Methods':'GET, POST, DELETE, PUT',
        'Access-Control-Allow-Headers':'X-Requests-With,content-type'
      }),
      observe: 'response' as 'response'
    };
    return this.httpClient.post(`${this.url}/validaAccount`,JSON.stringify(algo),options)
    .pipe(map(res=>res));
  }


}
