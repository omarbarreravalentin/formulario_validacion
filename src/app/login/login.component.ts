import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ComunicacionService } from '../comunicacion.service';
import { timeout } from 'rxjs/operators';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';
import { GuardService } from '../guard.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required])
  })

  get f() {
    return this.form.controls;
  }

  constructor(private rutas: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private guardService: GuardService,
    private comunica: ComunicacionService) { }

  ngOnInit(): void {
  }

  sumbit() {
    this.spinner.show();
    const datos = this.form.value;
    console.log(datos);
    this.comunica.validaAccount(datos)
      .pipe(timeout(20000))
      .subscribe((response) => {
        this.validaResponse(response.status, response);
      },
        (response) => {
          this.validaResponse(response.status, response);
        });
    //console.log("Intentas ingresar con estos datos "+JSON.stringify(datos));
  }

  validaResponse(code, response) {
    this.spinner.hide();
    switch (code) {
      case 200:
        this.guardService.setLogeado(true);
        this.rutas.navigate(['/dashboard']);
        break;
      default:
        this.guardService.setLogeado(false);
        this.toastr.error('Ups', 'Error, valide sus credenciales', {
          timeOut: 3000,
        });
        this.form.reset();
        //$('#myModal').modal('toggle');
        break;
    }
  }

  crearCuenta() {
    this.rutas.navigate(['/registro']);
  }

  // correoInvalido=false;


  // isValidEmail(mail) { 
  //   return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(mail); 
  // }



  // valida(event){
  //   const correo=event.target.value;
  //   const esvalido=this.isValidEmail(correo);
  //   if(esvalido){
  //     this.correoInvalido=false;
  //   }else{
  //     this.correoInvalido=true;
  //   }
  //   console.log("validando_correo "+esvalido);
  // }

}
